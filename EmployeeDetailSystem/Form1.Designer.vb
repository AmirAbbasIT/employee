﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.BunifuElipse1 = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        Me.BunifuGradientPanel1 = New Bunifu.Framework.UI.BunifuGradientPanel()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.BunifuGradientPanel2 = New Bunifu.Framework.UI.BunifuGradientPanel()
        Me.BunifuCustomLabel1 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BunifuCustomLabel2 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.DesComboBox = New Bunifu.Framework.UI.BunifuDropdown()
        Me.BasicPayText = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.NameText = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.BunifuCustomLabel7 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel6 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel5 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel4 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel3 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.SaveRecordButton = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.NetPayText = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.GrossText = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.HraText = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.DeductionText = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.DaText = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.CalculateSalaryButton = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.MessageLabel = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.DeleteText = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.DeleteButton = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuImageButton1 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.BunifuGradientPanel1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.BunifuImageButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BunifuElipse1
        '
        Me.BunifuElipse1.ElipseRadius = 5
        Me.BunifuElipse1.TargetControl = Me
        '
        'BunifuGradientPanel1
        '
        Me.BunifuGradientPanel1.BackgroundImage = CType(resources.GetObject("BunifuGradientPanel1.BackgroundImage"), System.Drawing.Image)
        Me.BunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuGradientPanel1.Controls.Add(Me.BunifuImageButton1)
        Me.BunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.BunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.DimGray
        Me.BunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.Black
        Me.BunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.Gray
        Me.BunifuGradientPanel1.GradientTopRight = System.Drawing.Color.Silver
        Me.BunifuGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.BunifuGradientPanel1.Name = "BunifuGradientPanel1"
        Me.BunifuGradientPanel1.Quality = 10
        Me.BunifuGradientPanel1.Size = New System.Drawing.Size(810, 28)
        Me.BunifuGradientPanel1.TabIndex = 0
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Me.BunifuGradientPanel1
        Me.BunifuDragControl1.Vertical = True
        '
        'BunifuGradientPanel2
        '
        Me.BunifuGradientPanel2.BackgroundImage = CType(resources.GetObject("BunifuGradientPanel2.BackgroundImage"), System.Drawing.Image)
        Me.BunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuGradientPanel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.BunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.Red
        Me.BunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.Maroon
        Me.BunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.Firebrick
        Me.BunifuGradientPanel2.GradientTopRight = System.Drawing.Color.Firebrick
        Me.BunifuGradientPanel2.Location = New System.Drawing.Point(0, 28)
        Me.BunifuGradientPanel2.Name = "BunifuGradientPanel2"
        Me.BunifuGradientPanel2.Quality = 10
        Me.BunifuGradientPanel2.Size = New System.Drawing.Size(11, 607)
        Me.BunifuGradientPanel2.TabIndex = 1
        '
        'BunifuCustomLabel1
        '
        Me.BunifuCustomLabel1.AutoSize = True
        Me.BunifuCustomLabel1.Font = New System.Drawing.Font("Montserrat", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.BunifuCustomLabel1.Location = New System.Drawing.Point(17, 31)
        Me.BunifuCustomLabel1.Name = "BunifuCustomLabel1"
        Me.BunifuCustomLabel1.Size = New System.Drawing.Size(166, 33)
        Me.BunifuCustomLabel1.TabIndex = 2
        Me.BunifuCustomLabel1.Text = "Employees"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DataGridView1.Location = New System.Drawing.Point(11, 437)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(799, 198)
        Me.DataGridView1.TabIndex = 3
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BunifuCustomLabel2)
        Me.GroupBox1.Controls.Add(Me.DesComboBox)
        Me.GroupBox1.Controls.Add(Me.BasicPayText)
        Me.GroupBox1.Controls.Add(Me.NameText)
        Me.GroupBox1.Location = New System.Drawing.Point(45, 85)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(508, 90)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Employee Detail"
        '
        'BunifuCustomLabel2
        '
        Me.BunifuCustomLabel2.AutoSize = True
        Me.BunifuCustomLabel2.Location = New System.Drawing.Point(313, 14)
        Me.BunifuCustomLabel2.Name = "BunifuCustomLabel2"
        Me.BunifuCustomLabel2.Size = New System.Drawing.Size(65, 13)
        Me.BunifuCustomLabel2.TabIndex = 3
        Me.BunifuCustomLabel2.Text = "Desigination"
        '
        'DesComboBox
        '
        Me.DesComboBox.BackColor = System.Drawing.Color.Transparent
        Me.DesComboBox.BorderRadius = 3
        Me.DesComboBox.DisabledColor = System.Drawing.Color.Gray
        Me.DesComboBox.ForeColor = System.Drawing.Color.White
        Me.DesComboBox.Items = New String() {"Manager", "Engineer", "Clerk"}
        Me.DesComboBox.Location = New System.Drawing.Point(316, 30)
        Me.DesComboBox.Name = "DesComboBox"
        Me.DesComboBox.NomalColor = System.Drawing.Color.LightSeaGreen
        Me.DesComboBox.onHoverColor = System.Drawing.Color.Teal
        Me.DesComboBox.selectedIndex = 0
        Me.DesComboBox.Size = New System.Drawing.Size(181, 35)
        Me.DesComboBox.TabIndex = 2
        '
        'BasicPayText
        '
        Me.BasicPayText.BackColor = System.Drawing.Color.White
        Me.BasicPayText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.BasicPayText.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.BasicPayText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BasicPayText.HintForeColor = System.Drawing.Color.Empty
        Me.BasicPayText.HintText = "Basic Pay"
        Me.BasicPayText.isPassword = False
        Me.BasicPayText.LineFocusedColor = System.Drawing.Color.Blue
        Me.BasicPayText.LineIdleColor = System.Drawing.Color.Gray
        Me.BasicPayText.LineMouseHoverColor = System.Drawing.Color.Blue
        Me.BasicPayText.LineThickness = 3
        Me.BasicPayText.Location = New System.Drawing.Point(162, 32)
        Me.BasicPayText.Margin = New System.Windows.Forms.Padding(4)
        Me.BasicPayText.Name = "BasicPayText"
        Me.BasicPayText.Size = New System.Drawing.Size(147, 33)
        Me.BasicPayText.TabIndex = 1
        Me.BasicPayText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'NameText
        '
        Me.NameText.BackColor = System.Drawing.Color.White
        Me.NameText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.NameText.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.NameText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.NameText.HintForeColor = System.Drawing.Color.Empty
        Me.NameText.HintText = "Name"
        Me.NameText.isPassword = False
        Me.NameText.LineFocusedColor = System.Drawing.Color.Blue
        Me.NameText.LineIdleColor = System.Drawing.Color.Gray
        Me.NameText.LineMouseHoverColor = System.Drawing.Color.Blue
        Me.NameText.LineThickness = 3
        Me.NameText.Location = New System.Drawing.Point(7, 32)
        Me.NameText.Margin = New System.Windows.Forms.Padding(4)
        Me.NameText.Name = "NameText"
        Me.NameText.Size = New System.Drawing.Size(147, 33)
        Me.NameText.TabIndex = 0
        Me.NameText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.MessageLabel)
        Me.GroupBox2.Controls.Add(Me.BunifuCustomLabel7)
        Me.GroupBox2.Controls.Add(Me.BunifuCustomLabel6)
        Me.GroupBox2.Controls.Add(Me.BunifuCustomLabel5)
        Me.GroupBox2.Controls.Add(Me.BunifuCustomLabel4)
        Me.GroupBox2.Controls.Add(Me.BunifuCustomLabel3)
        Me.GroupBox2.Controls.Add(Me.SaveRecordButton)
        Me.GroupBox2.Controls.Add(Me.NetPayText)
        Me.GroupBox2.Controls.Add(Me.GrossText)
        Me.GroupBox2.Controls.Add(Me.HraText)
        Me.GroupBox2.Controls.Add(Me.DeductionText)
        Me.GroupBox2.Controls.Add(Me.DaText)
        Me.GroupBox2.Location = New System.Drawing.Point(45, 214)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(508, 185)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Salary Detail"
        '
        'BunifuCustomLabel7
        '
        Me.BunifuCustomLabel7.AutoSize = True
        Me.BunifuCustomLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel7.Location = New System.Drawing.Point(104, 111)
        Me.BunifuCustomLabel7.Name = "BunifuCustomLabel7"
        Me.BunifuCustomLabel7.Size = New System.Drawing.Size(90, 26)
        Me.BunifuCustomLabel7.TabIndex = 11
        Me.BunifuCustomLabel7.Text = "Net Pay"
        '
        'BunifuCustomLabel6
        '
        Me.BunifuCustomLabel6.AutoSize = True
        Me.BunifuCustomLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel6.Location = New System.Drawing.Point(219, 68)
        Me.BunifuCustomLabel6.Name = "BunifuCustomLabel6"
        Me.BunifuCustomLabel6.Size = New System.Drawing.Size(70, 26)
        Me.BunifuCustomLabel6.TabIndex = 10
        Me.BunifuCustomLabel6.Text = "Gross"
        '
        'BunifuCustomLabel5
        '
        Me.BunifuCustomLabel5.AutoSize = True
        Me.BunifuCustomLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel5.Location = New System.Drawing.Point(25, 68)
        Me.BunifuCustomLabel5.Name = "BunifuCustomLabel5"
        Me.BunifuCustomLabel5.Size = New System.Drawing.Size(59, 26)
        Me.BunifuCustomLabel5.TabIndex = 9
        Me.BunifuCustomLabel5.Text = "HRA"
        '
        'BunifuCustomLabel4
        '
        Me.BunifuCustomLabel4.AutoSize = True
        Me.BunifuCustomLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel4.Location = New System.Drawing.Point(219, 27)
        Me.BunifuCustomLabel4.Name = "BunifuCustomLabel4"
        Me.BunifuCustomLabel4.Size = New System.Drawing.Size(110, 26)
        Me.BunifuCustomLabel4.TabIndex = 8
        Me.BunifuCustomLabel4.Text = "Deduction"
        '
        'BunifuCustomLabel3
        '
        Me.BunifuCustomLabel3.AutoSize = True
        Me.BunifuCustomLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel3.Location = New System.Drawing.Point(25, 27)
        Me.BunifuCustomLabel3.Name = "BunifuCustomLabel3"
        Me.BunifuCustomLabel3.Size = New System.Drawing.Size(43, 26)
        Me.BunifuCustomLabel3.TabIndex = 7
        Me.BunifuCustomLabel3.Text = "DA"
        '
        'SaveRecordButton
        '
        Me.SaveRecordButton.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SaveRecordButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SaveRecordButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SaveRecordButton.BorderRadius = 0
        Me.SaveRecordButton.ButtonText = "Save Record"
        Me.SaveRecordButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveRecordButton.DisabledColor = System.Drawing.Color.Gray
        Me.SaveRecordButton.Iconcolor = System.Drawing.Color.Transparent
        Me.SaveRecordButton.Iconimage = Nothing
        Me.SaveRecordButton.Iconimage_right = Nothing
        Me.SaveRecordButton.Iconimage_right_Selected = Nothing
        Me.SaveRecordButton.Iconimage_Selected = Nothing
        Me.SaveRecordButton.IconMarginLeft = 0
        Me.SaveRecordButton.IconMarginRight = 0
        Me.SaveRecordButton.IconRightVisible = True
        Me.SaveRecordButton.IconRightZoom = 0R
        Me.SaveRecordButton.IconVisible = True
        Me.SaveRecordButton.IconZoom = 90.0R
        Me.SaveRecordButton.IsTab = False
        Me.SaveRecordButton.Location = New System.Drawing.Point(337, 139)
        Me.SaveRecordButton.Name = "SaveRecordButton"
        Me.SaveRecordButton.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SaveRecordButton.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.SaveRecordButton.OnHoverTextColor = System.Drawing.Color.White
        Me.SaveRecordButton.selected = False
        Me.SaveRecordButton.Size = New System.Drawing.Size(160, 40)
        Me.SaveRecordButton.TabIndex = 6
        Me.SaveRecordButton.Text = "Save Record"
        Me.SaveRecordButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveRecordButton.Textcolor = System.Drawing.Color.White
        Me.SaveRecordButton.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'NetPayText
        '
        Me.NetPayText.BackColor = System.Drawing.Color.White
        Me.NetPayText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.NetPayText.Enabled = False
        Me.NetPayText.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.NetPayText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.NetPayText.HintForeColor = System.Drawing.Color.Empty
        Me.NetPayText.HintText = ""
        Me.NetPayText.isPassword = False
        Me.NetPayText.LineFocusedColor = System.Drawing.Color.Blue
        Me.NetPayText.LineIdleColor = System.Drawing.Color.Gray
        Me.NetPayText.LineMouseHoverColor = System.Drawing.Color.Blue
        Me.NetPayText.LineThickness = 3
        Me.NetPayText.Location = New System.Drawing.Point(200, 104)
        Me.NetPayText.Margin = New System.Windows.Forms.Padding(4)
        Me.NetPayText.Name = "NetPayText"
        Me.NetPayText.Size = New System.Drawing.Size(109, 33)
        Me.NetPayText.TabIndex = 5
        Me.NetPayText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'GrossText
        '
        Me.GrossText.BackColor = System.Drawing.Color.White
        Me.GrossText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.GrossText.Enabled = False
        Me.GrossText.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.GrossText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GrossText.HintForeColor = System.Drawing.Color.Empty
        Me.GrossText.HintText = ""
        Me.GrossText.isPassword = False
        Me.GrossText.LineFocusedColor = System.Drawing.Color.Blue
        Me.GrossText.LineIdleColor = System.Drawing.Color.Gray
        Me.GrossText.LineMouseHoverColor = System.Drawing.Color.Blue
        Me.GrossText.LineThickness = 3
        Me.GrossText.Location = New System.Drawing.Point(347, 61)
        Me.GrossText.Margin = New System.Windows.Forms.Padding(4)
        Me.GrossText.Name = "GrossText"
        Me.GrossText.Size = New System.Drawing.Size(109, 33)
        Me.GrossText.TabIndex = 4
        Me.GrossText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'HraText
        '
        Me.HraText.BackColor = System.Drawing.Color.White
        Me.HraText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.HraText.Enabled = False
        Me.HraText.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.HraText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.HraText.HintForeColor = System.Drawing.Color.Empty
        Me.HraText.HintText = ""
        Me.HraText.isPassword = False
        Me.HraText.LineFocusedColor = System.Drawing.Color.Blue
        Me.HraText.LineIdleColor = System.Drawing.Color.Gray
        Me.HraText.LineMouseHoverColor = System.Drawing.Color.Blue
        Me.HraText.LineThickness = 3
        Me.HraText.Location = New System.Drawing.Point(85, 61)
        Me.HraText.Margin = New System.Windows.Forms.Padding(4)
        Me.HraText.Name = "HraText"
        Me.HraText.Size = New System.Drawing.Size(109, 33)
        Me.HraText.TabIndex = 3
        Me.HraText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'DeductionText
        '
        Me.DeductionText.BackColor = System.Drawing.Color.White
        Me.DeductionText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.DeductionText.Enabled = False
        Me.DeductionText.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.DeductionText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DeductionText.HintForeColor = System.Drawing.Color.Empty
        Me.DeductionText.HintText = ""
        Me.DeductionText.isPassword = False
        Me.DeductionText.LineFocusedColor = System.Drawing.Color.Blue
        Me.DeductionText.LineIdleColor = System.Drawing.Color.Gray
        Me.DeductionText.LineMouseHoverColor = System.Drawing.Color.Blue
        Me.DeductionText.LineThickness = 3
        Me.DeductionText.Location = New System.Drawing.Point(347, 20)
        Me.DeductionText.Margin = New System.Windows.Forms.Padding(4)
        Me.DeductionText.Name = "DeductionText"
        Me.DeductionText.Size = New System.Drawing.Size(109, 33)
        Me.DeductionText.TabIndex = 2
        Me.DeductionText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'DaText
        '
        Me.DaText.BackColor = System.Drawing.Color.White
        Me.DaText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.DaText.Enabled = False
        Me.DaText.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.DaText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DaText.HintForeColor = System.Drawing.Color.Empty
        Me.DaText.HintText = ""
        Me.DaText.isPassword = False
        Me.DaText.LineFocusedColor = System.Drawing.Color.Blue
        Me.DaText.LineIdleColor = System.Drawing.Color.Gray
        Me.DaText.LineMouseHoverColor = System.Drawing.Color.Blue
        Me.DaText.LineThickness = 3
        Me.DaText.Location = New System.Drawing.Point(85, 20)
        Me.DaText.Margin = New System.Windows.Forms.Padding(4)
        Me.DaText.Name = "DaText"
        Me.DaText.Size = New System.Drawing.Size(109, 33)
        Me.DaText.TabIndex = 1
        Me.DaText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'CalculateSalaryButton
        '
        Me.CalculateSalaryButton.Activecolor = System.Drawing.Color.DarkCyan
        Me.CalculateSalaryButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CalculateSalaryButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CalculateSalaryButton.BorderRadius = 0
        Me.CalculateSalaryButton.ButtonText = "Calculate Salary"
        Me.CalculateSalaryButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CalculateSalaryButton.DisabledColor = System.Drawing.Color.Gray
        Me.CalculateSalaryButton.Iconcolor = System.Drawing.Color.Transparent
        Me.CalculateSalaryButton.Iconimage = Nothing
        Me.CalculateSalaryButton.Iconimage_right = Nothing
        Me.CalculateSalaryButton.Iconimage_right_Selected = Nothing
        Me.CalculateSalaryButton.Iconimage_Selected = Nothing
        Me.CalculateSalaryButton.IconMarginLeft = 0
        Me.CalculateSalaryButton.IconMarginRight = 0
        Me.CalculateSalaryButton.IconRightVisible = True
        Me.CalculateSalaryButton.IconRightZoom = 0R
        Me.CalculateSalaryButton.IconVisible = True
        Me.CalculateSalaryButton.IconZoom = 90.0R
        Me.CalculateSalaryButton.IsTab = False
        Me.CalculateSalaryButton.Location = New System.Drawing.Point(402, 181)
        Me.CalculateSalaryButton.Name = "CalculateSalaryButton"
        Me.CalculateSalaryButton.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CalculateSalaryButton.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.CalculateSalaryButton.OnHoverTextColor = System.Drawing.Color.White
        Me.CalculateSalaryButton.selected = False
        Me.CalculateSalaryButton.Size = New System.Drawing.Size(151, 36)
        Me.CalculateSalaryButton.TabIndex = 6
        Me.CalculateSalaryButton.Text = "Calculate Salary"
        Me.CalculateSalaryButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CalculateSalaryButton.Textcolor = System.Drawing.Color.White
        Me.CalculateSalaryButton.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'MessageLabel
        '
        Me.MessageLabel.AutoSize = True
        Me.MessageLabel.Location = New System.Drawing.Point(7, 165)
        Me.MessageLabel.Name = "MessageLabel"
        Me.MessageLabel.Size = New System.Drawing.Size(53, 13)
        Me.MessageLabel.TabIndex = 12
        Me.MessageLabel.Text = "Message:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.DeleteButton)
        Me.GroupBox3.Controls.Add(Me.DeleteText)
        Me.GroupBox3.Location = New System.Drawing.Point(569, 85)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(220, 132)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Delete Record"
        '
        'DeleteText
        '
        Me.DeleteText.BackColor = System.Drawing.Color.White
        Me.DeleteText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.DeleteText.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.DeleteText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DeleteText.HintForeColor = System.Drawing.Color.Empty
        Me.DeleteText.HintText = "Employee ID"
        Me.DeleteText.isPassword = False
        Me.DeleteText.LineFocusedColor = System.Drawing.Color.Blue
        Me.DeleteText.LineIdleColor = System.Drawing.Color.Gray
        Me.DeleteText.LineMouseHoverColor = System.Drawing.Color.Blue
        Me.DeleteText.LineThickness = 3
        Me.DeleteText.Location = New System.Drawing.Point(7, 20)
        Me.DeleteText.Margin = New System.Windows.Forms.Padding(4)
        Me.DeleteText.Name = "DeleteText"
        Me.DeleteText.Size = New System.Drawing.Size(206, 34)
        Me.DeleteText.TabIndex = 1
        Me.DeleteText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'DeleteButton
        '
        Me.DeleteButton.Activecolor = System.Drawing.Color.DarkCyan
        Me.DeleteButton.BackColor = System.Drawing.Color.Red
        Me.DeleteButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.DeleteButton.BorderRadius = 0
        Me.DeleteButton.ButtonText = "Delete Employee"
        Me.DeleteButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteButton.DisabledColor = System.Drawing.Color.Gray
        Me.DeleteButton.Iconcolor = System.Drawing.Color.Transparent
        Me.DeleteButton.Iconimage = Nothing
        Me.DeleteButton.Iconimage_right = Nothing
        Me.DeleteButton.Iconimage_right_Selected = Nothing
        Me.DeleteButton.Iconimage_Selected = Nothing
        Me.DeleteButton.IconMarginLeft = 0
        Me.DeleteButton.IconMarginRight = 0
        Me.DeleteButton.IconRightVisible = True
        Me.DeleteButton.IconRightZoom = 0R
        Me.DeleteButton.IconVisible = True
        Me.DeleteButton.IconZoom = 90.0R
        Me.DeleteButton.IsTab = False
        Me.DeleteButton.Location = New System.Drawing.Point(7, 71)
        Me.DeleteButton.Name = "DeleteButton"
        Me.DeleteButton.Normalcolor = System.Drawing.Color.Red
        Me.DeleteButton.OnHovercolor = System.Drawing.Color.Firebrick
        Me.DeleteButton.OnHoverTextColor = System.Drawing.Color.White
        Me.DeleteButton.selected = False
        Me.DeleteButton.Size = New System.Drawing.Size(206, 36)
        Me.DeleteButton.TabIndex = 7
        Me.DeleteButton.Text = "Delete Employee"
        Me.DeleteButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DeleteButton.Textcolor = System.Drawing.Color.White
        Me.DeleteButton.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'BunifuImageButton1
        '
        Me.BunifuImageButton1.BackColor = System.Drawing.Color.Transparent
        Me.BunifuImageButton1.Image = Global.EmployeeDetailSystem.My.Resources.Resources.close
        Me.BunifuImageButton1.ImageActive = Nothing
        Me.BunifuImageButton1.Location = New System.Drawing.Point(777, 0)
        Me.BunifuImageButton1.Name = "BunifuImageButton1"
        Me.BunifuImageButton1.Size = New System.Drawing.Size(30, 28)
        Me.BunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuImageButton1.TabIndex = 8
        Me.BunifuImageButton1.TabStop = False
        Me.BunifuImageButton1.Zoom = 10
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(810, 635)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.CalculateSalaryButton)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.BunifuCustomLabel1)
        Me.Controls.Add(Me.BunifuGradientPanel2)
        Me.Controls.Add(Me.BunifuGradientPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form1"
        Me.ShowIcon = False
        Me.Text = "SimpleEmployeeDetail"
        Me.BunifuGradientPanel1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.BunifuImageButton1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BunifuElipse1 As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents BunifuGradientPanel1 As Bunifu.Framework.UI.BunifuGradientPanel
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
    Friend WithEvents BunifuGradientPanel2 As Bunifu.Framework.UI.BunifuGradientPanel
    Friend WithEvents BunifuCustomLabel1 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents DesComboBox As Bunifu.Framework.UI.BunifuDropdown
    Friend WithEvents BasicPayText As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents NameText As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents BunifuCustomLabel2 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel7 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel6 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel5 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel4 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel3 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents SaveRecordButton As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents NetPayText As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents GrossText As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents HraText As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents DeductionText As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents DaText As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents CalculateSalaryButton As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents MessageLabel As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents DeleteText As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents DeleteButton As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuImageButton1 As Bunifu.Framework.UI.BunifuImageButton
End Class
