﻿Imports System.Data.SqlClient
Public Class Form1

    Public isPayCalculated As Boolean = False


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        UpdateGrid()

    End Sub


    Public Sub UpdateGrid()
        Dim connectionString As String = "Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=EmployeeDB;Integrated Security=True;"
        Dim sql As String = "SELECT * FROM EMPDetail"
        Dim connection As New SqlConnection(connectionString)
        Dim dataadapter As New SqlDataAdapter(sql, connection)
        Dim ds As New DataSet()
        connection.Open()
        dataadapter.Fill(ds, "EMPDetail")
        connection.Close()
        DataGridView1.DataSource = ds.Tables(0)
    End Sub



    Private Sub BasicPayText_KeyPress(sender As Object, e As KeyPressEventArgs) Handles BasicPayText.KeyPress
        If (Microsoft.VisualBasic.Asc(e.KeyChar) < 48) _
                  Or (Microsoft.VisualBasic.Asc(e.KeyChar) > 57) Then
            e.Handled = True
        End If
        If (Microsoft.VisualBasic.Asc(e.KeyChar) = 8) Then
            e.Handled = False
        End If
    End Sub

    Private Sub CalculateSalaryButton_Click(sender As Object, e As EventArgs) Handles CalculateSalaryButton.Click
        Select Case DesComboBox.selectedValue
            Case "Manager"
                If BasicPayText.Text >= 15000 And BasicPayText.Text <= 25000 Then
                    DaText.Text = BasicPayText.Text * 0.08
                    HraText.Text = BasicPayText.Text * 0.03
                    DeductionText.Text = BasicPayText.Text * 0.01
                    GrossText.Text = CType(BasicPayText.Text, Decimal) + CType(DaText.Text, Decimal) + CType(HraText.Text, Decimal)
                    NetPayText.Text = CType(GrossText.Text, Decimal) - CType(DeductionText.Text, Decimal)
                Else
                    MessageLabel.Text = "Enter Basic Value between 15000 and 25000"
                End If
            Case "Engineer"
                If BasicPayText.Text >= 15000 And BasicPayText.Text <= 20000 Then
                    DaText.Text = BasicPayText.Text * 0.07
                    HraText.Text = BasicPayText.Text * 0.04
                    DeductionText.Text = BasicPayText.Text * 0.01
                    GrossText.Text = CType(BasicPayText.Text, Decimal) + CType(DaText.Text, Decimal) + CType(HraText.Text, Decimal)
                    NetPayText.Text = CType(GrossText.Text, Decimal) - CType(DeductionText.Text, Decimal)
                Else
                    MessageLabel.Text = "Enter Basic Value between 15000 and 20000"
                End If
            Case "Clerk"
                If BasicPayText.Text >= 5000 And BasicPayText.Text <= 15000 Then
                    DaText.Text = BasicPayText.Text * 0.04
                    HraText.Text = BasicPayText.Text * 0.02
                    DeductionText.Text = BasicPayText.Text * 0.01
                    GrossText.Text = CType(BasicPayText.Text, Decimal) + CType(DaText.Text, Decimal) + CType(HraText.Text, Decimal)
                    NetPayText.Text = CType(GrossText.Text, Decimal) - CType(DeductionText.Text, Decimal)
                Else
                    MessageLabel.Text = "Enter Basic Value between 5000 and 15000"
                End If
        End Select
        isPayCalculated = True
    End Sub

    Private Sub SaveRecordButton_Click(sender As Object, e As EventArgs) Handles SaveRecordButton.Click
        If isPayCalculated Then
            Dim connectionString As String = "Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=EmployeeDB;Integrated Security=True;"
            Dim connection As New SqlConnection(connectionString)
            Dim cmd As SqlCommand = New SqlCommand
            cmd.Connection = connection
            cmd.CommandText = "Insert into EMPDetail Values(@Name,@BasicPay,@Da,@Hra,@Deduction,@Gross,@NetPay)"
            cmd.Parameters.Add("@Name", NameText.Text)
            cmd.Parameters.Add("@BasicPay", CType(BasicPayText.Text, Decimal))
            cmd.Parameters.Add("@Da", CType(DaText.Text, Decimal))
            cmd.Parameters.Add("@Hra", CType(HraText.Text, Decimal))
            cmd.Parameters.Add("@Deduction", CType(DeductionText.Text, Decimal))
            cmd.Parameters.Add("@Gross", CType(GrossText.Text, Decimal))
            cmd.Parameters.Add("@NetPay", CType(NetPayText.Text, Decimal))
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()

            'Now Updating GridView....
            UpdateGrid()
            NameText.Text = ""
            BasicPayText.Text = ""
            DaText.Text = ""
            HraText.Text = ""
            DeductionText.Text = ""
            GrossText.Text = ""
            NetPayText.Text = ""
            'Now .....
            isPayCalculated = False
            MessageLabel.Text = "Record Saved in Database..."
        Else
            MessageLabel.Text = "Calculate Salary First..."
        End If

    End Sub

    Private Sub DeleteButton_Click(sender As Object, e As EventArgs) Handles DeleteButton.Click
        Dim connectionString As String = "Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=EmployeeDB;Integrated Security=True;"
        Dim connection As New SqlConnection(connectionString)
        Dim cmd As SqlCommand = New SqlCommand
        cmd.Connection = connection
        cmd.CommandText = "Delete from EMPDetail where ID=@EmpID"
        cmd.Parameters.Add("@EmpID", CType(DeleteText.Text, Integer))
        connection.Open()
        Dim result As Integer = cmd.ExecuteNonQuery()
        connection.Close()
        'Now Updating GridView....
        UpdateGrid()
        If result <> 0 Then
            MessageLabel.Text = "Employee Deleted Successfuly"
        Else
            MessageLabel.Text = "Employee not Found"
        End If

    End Sub

    Private Sub BunifuImageButton1_Click(sender As Object, e As EventArgs) Handles BunifuImageButton1.Click
        Application.Exit()
    End Sub
End Class
