USE [EmployeeDB]
GO

/****** Object: Table [dbo].[EMPDetail] Script Date: 5/22/2019 8:46:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EMPDetail] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (50) NULL,
    [Basic Pay] DECIMAL (18)  NULL,
    [Da]        DECIMAL (18)  NULL,
    [Hra]       DECIMAL (18)  NULL,
    [Deduction] DECIMAL (18)  NULL,
    [Gross]     DECIMAL (18)  NULL,
    [NetPay]    DECIMAL (18)  NULL
);


